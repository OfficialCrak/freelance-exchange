from django.shortcuts import render, get_object_or_404
from .models import Forum, Comment
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from .serializers import ForumSerializers, ForumDetailSerializers, ForumPostSerializers, CommentSerializer, CommentPostSerializer
from rest_framework.views import APIView
from rest_framework import permissions, status
from rest_framework.response import Response


class ApiForum(APIView):
    permission_classes = [permissions.IsAuthenticatedOrReadOnly, ]
    serializer_class = ForumPostSerializers

    def get(self, request):
        forum = Forum.objects.all()
        serializer = ForumSerializers(forum, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save(created_by=request.user)
            return Response(data=serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ApiDetailForum(APIView):
    permission_classes = [permissions.IsAuthenticatedOrReadOnly, ]

    def get(self, request, pk):
        forum = Forum.objects.get(id=pk)
        serializers = ForumDetailSerializers(forum)

        return Response(serializers.data, status=status.HTTP_200_OK)


class ApiPostComment(APIView):
    permission_classes = [permissions.IsAuthenticatedOrReadOnly, ]
    serializer_class = CommentPostSerializer
    model = Comment

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save(user=request.user)
            return Response(data=serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
