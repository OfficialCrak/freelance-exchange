from django.urls import path
from .views import ApiForum, ApiDetailForum, ApiPostComment

urlpatterns = [
    path('', ApiForum.as_view()),
    path('<int:pk>/', ApiDetailForum.as_view()),
    path('comment/', ApiPostComment.as_view()),
]