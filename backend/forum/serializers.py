from rest_framework  import serializers
from .models import Forum, Comment
from django.contrib.auth.models import User


class UserSerializers(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username')


class ForumSerializers(serializers.ModelSerializer):
    created_by = UserSerializers()

    class Meta:
        model = Forum
        fields = ('id', 'title', 'description', 'created_at', 'created_by')


class ForumPostSerializers(serializers.ModelSerializer):
    title = serializers.CharField(max_length=200)
    description = serializers.CharField()

    class Meta:
        model = Forum
        fields = ('title', 'description')


class CommentSerializer(serializers.ModelSerializer):
    user = UserSerializers()

    class Meta:
        model = Comment
        fields = '__all__'


class ForumDetailSerializers(serializers.ModelSerializer):
    created_by = UserSerializers()
    comment = CommentSerializer(many=True)

    class Meta:
        model = Forum
        fields = "__all__"


class CommentPostSerializer(serializers.ModelSerializer):
    post = serializers.SlugRelatedField(slug_field='id', queryset=Forum.objects)
    text = serializers.CharField()

    class Meta:
        model = Comment
        fields = ('post', 'text')
