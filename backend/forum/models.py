from django.db import models
from django.contrib.auth.models import User


class Forum(models.Model):
    title = models.CharField(max_length=200, verbose_name='Тема обсуждения')
    description = models.TextField(verbose_name='Описание темы')
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Автор обсуждения')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')

    class Meta:
        verbose_name_plural = 'Обсуждения форума'
        verbose_name = 'Обсуждения'
        ordering = ['-created_at']

    def __str__(self):
        return self.title


class Comment(models.Model):
    post = models.ForeignKey(Forum, on_delete=models.CASCADE, verbose_name='ID поста', related_name='comment')
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Отправитель')
    text = models.CharField(max_length=200, blank=False, verbose_name='Сообщение')
    create_at = models.DateTimeField(auto_now_add=True, db_index=True, verbose_name='Отправлено')

    class Meta:
        verbose_name_plural = 'Комментарии поста'
        verbose_name = 'Комментарии'
        ordering = ['-create_at']

    def __str__(self):
        return self.text
