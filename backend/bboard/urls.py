import requests
from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url
from .views import APIBoard, APICategory, APIUser, APIDetailBoard


urlpatterns = [
    # post views
    path('', APIBoard.as_view()),
    path('<int:pk>/', APIDetailBoard.as_view()),
    path('category/', APICategory.as_view()),
    path('user/', APIUser.as_view()),
]