from django.db import models
from django.contrib.auth.models import User


class Bb(models.Model):
    title = models.CharField(max_length=50, verbose_name='Тема объявления')
    content = models.TextField(null=True, blank=True, verbose_name='Описание темы')
    price = models.FloatField(null=True, blank=True, verbose_name='Цена')
    published = models.DateTimeField(auto_now_add=True, db_index=True, verbose_name='Опубликовано')
    category = models.ForeignKey('Category', null=True, on_delete=models.PROTECT, verbose_name='Категория')
    author = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Автор объявления')

    class Meta:
        verbose_name_plural = 'Объявления'
        verbose_name = 'Объявление'
        ordering = ['-published']

    def __str__(self):
        return self.title


class Category(models.Model):
    category = models.CharField(max_length=20, db_index=True, verbose_name='Название')

    def __str__(self):
        return self.category


    class Meta:
        verbose_name_plural = 'Категории'
        verbose_name = 'Категория'
        ordering = ['category']
