from django.http import HttpResponse
from rest_framework import permissions, status
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from rest_framework.decorators import api_view
from .models import Category, Bb
from django.contrib.auth.models import User
from .serializers import CategorySerializers, BbSerializers, UserSerializers, BbPostSerializers, BbDetailSerializers
from rest_framework.views import APIView


class APICategory(APIView):
    def get(self, request):
        category = Category.objects.all()
        serializer = CategorySerializers(category, many=True)
        return Response(serializer.data)

    def post(self, request):
        category = CategorySerializers(data=request.data)
        if category.is_valid():
            category.save()
            return Response(category.data, status=status.HTTP_201_CREATED)
        else:
            Response(category.errors, status=status.HTTP_400_BAD_REQUEST)


class APIBoard(APIView):
    permission_classes = [permissions.IsAuthenticatedOrReadOnly, ]
    serializer_class = BbPostSerializers
    model = Bb

    def get(self, request):
        bb = Bb.objects.all()
        serializer = BbSerializers(bb, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save(author=request.user)
            return Response(data=serializer.data, status=status.HTTP_201_CREATED)


class APIDetailBoard(APIView):
    permission_classes = [permissions.IsAuthenticatedOrReadOnly, ]

    def get(self, request, pk):
        bb = Bb.objects.get(id=pk)
        serializers = BbDetailSerializers(bb)
        return Response(serializers.data)

    def delete(self, request, pk):
        bb = Bb.objects.get(id=pk)
        user = bb.author
        author = request.user
        if user == author:
            bb.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)

    def put(self, request, pk):
        bb_update = Bb.objects.get(id=pk)
        serializer = BbPostSerializers(bb_update, data=request.data)
        user = bb_update.author
        author = request.user
        if user == author:
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class APIUser(APIView):
    def get(self, request):
        user = User.objects.all()
        serializer = UserSerializers(user, many=True)
        return Response(serializer.data)
