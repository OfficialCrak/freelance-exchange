from rest_framework import serializers
from .models import Bb, Category
from django.contrib.auth.models import User


class UserSerializers(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username')


class CategorySerializers(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'category')


class BbSerializers(serializers.ModelSerializer):
    author = UserSerializers()
    category = CategorySerializers()

    class Meta:
        model = Bb
        fields = ('id', 'title', 'content', 'price', 'published', 'category', 'author')


class BbPostSerializers(serializers.ModelSerializer):
    title = serializers.CharField(max_length=50)
    content = serializers.CharField()
    price = serializers.FloatField()
    category = serializers.SlugRelatedField(slug_field='category', queryset=Category.objects)


    class Meta:
        model = Bb
        fields = ('title', 'content', 'price', 'category')

    def create(self, validated_data):
        return Bb.objects.create(**validated_data)


class BbDetailSerializers(serializers.ModelSerializer):
    author = UserSerializers()
    category = CategorySerializers()

    class Meta:
        model = Bb
        fields = '__all__'